# Vasuki PQC #



### What is Vasuki PQC? ###

**Vasuki PQC** (**P**ost **Q**uantum **C**ryptography) is a **PyTorch** library fortified with **NIST** (**N**ational **I**nstitute of **S**tandards and **T**echnology) 
public key cryptography algorithms that are secure against attack by quantum computers in the near future.

**Vasuki PQC** will incorporate proposed **NIST** **KEM**s (**K**ey **E**ncapsulation **M**echanisms) such as **Kyber**, **NTRU**, & **SABER**. 